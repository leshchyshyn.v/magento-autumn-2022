<?php

namespace Academy\Example\Block;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Api\Data\ProductInterface as ProductInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;

class ProductInfo extends Template
{
    private ProductInterface $product;

    /**
     * @param Template\Context $context
     * @param ProductInterface $product
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ProductInterface $product,
        array $data = []
    )
    {

        parent::__construct($context, $data);
        $this->product = $product;
    }

    public function getProductInfo()
    {
//        $objectManager = ObjectManager::getInstance();
//        $product = $objectManager->get(Product::class);

        echo get_class($this->product);

        $this->product->load(1);

//        $this->product->getData("name");
//
//
//        $this->product->setBlaBla("some info");
//
//        $this->product->setName("Me Awesome Product");
//        $this->product->setDame("name","Me Awesome Product");

        return $this->product->getName();
    }
}
