<?php

namespace Academy\ImportExport\Console\Command;

use Academy\ImportExport\Service\Import;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportExport extends Command
{
    private array $importers;

    /**
     * @param array $importers
     * @param string|null $name
     */
    public function __construct(
        array $importers = [],
        string $name = null
    )
    {
        parent::__construct($name);
        $this->importers = $importers;
    }

    public function configure()
    {
        $this->setName("academy:importexport");
        $this->setDescription("Import or export operation");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->importers as $importer)
        {
            $importer->execute();
        }
    }
}
