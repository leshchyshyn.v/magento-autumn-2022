<?php

namespace Academy\ImportExport\Service;

class Import
{
    private string $filename;

    /**
     * @param string $filename
     */
    public function __construct(
        string $filename = "import.csv"
    )
    {

        $this->filename = $filename;
    }


    public function execute()
    {
        echo "Importing ".$this->filename."\n";
    }
}
