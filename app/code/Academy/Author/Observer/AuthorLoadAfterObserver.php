<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Academy\Author\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AuthorLoadAfterObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
//        $author = $observer->getAuthor();
//
//        $author->setFirstname($author->getFirstname()." observerd");
    }
}
