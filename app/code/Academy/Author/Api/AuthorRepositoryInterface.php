<?php
declare(strict_types=1);

namespace Academy\Author\Api;

use Academy\Author\Api\Data\AuthorInterface;

interface AuthorRepositoryInterface
{
    /**
     * @param int $id
     * @return AuthorInterface
     */
    public function get(int $id) : AuthorInterface;

    /**
     * @param AuthorInterface $author
     * @return AuthorInterface
     */
    public function save(AuthorInterface $author) : AuthorInterface;

    /**
     * @param AuthorInterface $author
     * @return bool
     */
    public function delete(AuthorInterface $author) : bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id) : bool;
}
