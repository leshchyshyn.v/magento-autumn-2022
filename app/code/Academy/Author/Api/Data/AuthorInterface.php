<?php

namespace Academy\Author\Api\Data;

interface AuthorInterface
{
    const AUTHOR_ID     = 'author_id';
    const FIRSTNAME     = 'firstname';
    const LASTNAME      = 'lastname';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getFirstname();

    /**
     * @return string
     */
    public function getLastname();

    /**
     * @return string
     */
    public function getCreationTime();

    /**
     * @return string
     */
    public function getUpdateTime();

    /**
     * @param $id
     * @return AuthorInterface
     */
    public function setId($id);

    /**
     * @param $firstname
     * @return AuthorInterface
     */
    public function setFirstname($firstname);

    /**
     * @param $lastname
     * @return AuthorInterface
     */
    public function setLastname($lastname);

    /**
     * @param $time
     * @return AuthorInterface
     */
    public function setCreationTime($time);

    /**
     * @param $time
     * @return AuthorInterface
     */
    public function setUpdateTime($time);

}
