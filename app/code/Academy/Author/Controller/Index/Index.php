<?php

namespace Academy\Author\Controller\Index;

use Academy\Author\Api\AuthorRepositoryInterface;
use Academy\Author\Api\Data\AuthorInterfaceFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;
use Academy\Author\Model\ResourceModel\Author as AuthorResource;
use Academy\Author\Model\ResourceModel\Author\CollectionFactory as AuthorCollectionFactory;
use Magento\Store\Api\StoreManagementInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Index implements HttpGetActionInterface
{
    private PageFactory $pageFactory;
    private AuthorInterfaceFactory $authorFactory;
    private AuthorResource $authorResource;
    private AuthorCollectionFactory $collectionFactory;
    private AuthorRepositoryInterface $authorRepository;
    private ScopeConfigInterface $scopeConfig;
    private StoreManagementInterface $storeManagement;
    private StoreManagerInterface $storeManager;

    /**
     * @param PageFactory $pageFactory
     * @param AuthorInterfaceFactory $authorFactory
     * @param AuthorResource $authorResource
     * @param AuthorCollectionFactory $collectionFactory
     * @param AuthorRepositoryInterface $authorRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        PageFactory $pageFactory,
        AuthorInterfaceFactory $authorFactory,
        AuthorResource $authorResource,
        AuthorCollectionFactory $collectionFactory,
        AuthorRepositoryInterface $authorRepository,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    )
    {
        $this->pageFactory = $pageFactory;
        $this->authorFactory = $authorFactory;
        $this->authorResource = $authorResource;
        $this->collectionFactory = $collectionFactory;
        $this->authorRepository = $authorRepository;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    public function execute()
    {

        $title = $this->scopeConfig->isSetFlag(
            "author/general/enabled",
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        var_dump($title);

//        $author = $this->authorFactory->create();
//        $author->setFirstname("Oles")->setLastname("Honchar");
//
//
//        var_dump($author->getData());

//        $this->authorRepository->save($author);

//        echo $author->getId();

//        $collection = $this->collectionFactory->create();

//        $collection->addFieldToFilter(
//            "lastname", [
//                "in" => ["Franko", "Shevchencko"]
//            ]
//        );
//
//        echo $collection->getSelect()->__toString();

//        $author = $collection->getAllIds();


//        foreach ($collection as $author) {
//            echo $author->getFirstname()." ".$author->getLastname(). "<br/>";
//        }


//        $author = $this->authorFactory->create();

//        $author->setFirstname("Grygory")
//            ->setLastname("Skovoroda");

//        $this->authorResource->save($author);
//        $this->authorResource->load($author,3);

//        var_dump($author->getData());

//        $author->save();
        die;
        return $this->pageFactory->create();
    }
}
