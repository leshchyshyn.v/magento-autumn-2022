<?php

namespace Academy\Author\Controller\Adminhtml\Author;

use Academy\Author\Api\AuthorRepositoryInterface;
use Academy\Author\Api\Data\AuthorInterfaceFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\View\Result\PageFactory;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Academy_Author::Author';
    private PageFactory $pageFactory;
    private AuthorInterfaceFactory $authorFactory;
    private AuthorRepositoryInterface $authorRepository;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param AuthorInterfaceFactory $authorFactory
     * @param AuthorRepositoryInterface $authorRepository
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        AuthorInterfaceFactory $authorFactory,
        AuthorRepositoryInterface $authorRepository
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->authorFactory = $authorFactory;
        $this->authorRepository = $authorRepository;
    }

    public function execute()
    {
        $postData = $this->getRequest()->getParams();

        $author = $this->authorFactory->create();

        $author->setData($postData);

        if(!$author->getId()) {
            $author->unsetData("author_id");
        }

        try {
            $this->authorRepository->save($author);
            $this->messageManager->addSuccessMessage("Author Saved");
        } catch (CouldNotSaveException $ex) {
            $this->messageManager->addErrorMessage("Could not save author");
        }

        $result = $this->resultRedirectFactory->create();
        $result->setPath("*/*/");

        return $result;
    }
}
