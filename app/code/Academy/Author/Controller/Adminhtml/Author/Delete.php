<?php

namespace Academy\Author\Controller\Adminhtml\Author;

use Academy\Author\Api\AuthorRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Academy_Author::Author';
    private PageFactory $pageFactory;
    private AuthorRepositoryInterface $authorRepository;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param AuthorRepositoryInterface $authorRepository
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        AuthorRepositoryInterface $authorRepository
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->authorRepository = $authorRepository;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam("id");
        if($id) {
            try {
                $this->authorRepository->deleteById($id);
                $this->messageManager->addSuccessMessage("Author Deleted");
            } catch (CouldNotDeleteException $ex) {
                $this->messageManager->addErrorMessage("Failed to delete");
            } catch (NoSuchEntityException $exception) {
                $this->messageManager->addErrorMessage("Failed to delete");
            }
        }

        $result = $this->resultRedirectFactory->create();

        $result->setPath("*/*/");

        return $result;
    }
}
