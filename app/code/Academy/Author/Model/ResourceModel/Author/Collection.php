<?php

namespace Academy\Author\Model\ResourceModel\Author;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            \Academy\Author\Model\Author::class,
            \Academy\Author\Model\ResourceModel\Author::class
        );
    }
}
