<?php

namespace Academy\Author\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Author extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('academy_author','author_id');
    }
}
