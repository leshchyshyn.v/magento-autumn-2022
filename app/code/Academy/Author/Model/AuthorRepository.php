<?php
declare(strict_types=1);

namespace Academy\Author\Model;

use Academy\Author\Api\AuthorRepositoryInterface;
use Academy\Author\Api\Data\AuthorInterface;
use Academy\Author\Api\Data\AuthorInterfaceFactory;
use Academy\Author\Model\ResourceModel\Author as AuthorResource;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class AuthorRepository implements AuthorRepositoryInterface
{
    private AuthorResource $authorResource;
    private AuthorInterfaceFactory $authorFactory;

    /**
     * @param AuthorResource $authorResource
     * @param AuthorInterfaceFactory $authorFactory
     */
    public function __construct(
        AuthorResource $authorResource,
        AuthorInterfaceFactory $authorFactory
    )
    {
        $this->authorResource = $authorResource;
        $this->authorFactory = $authorFactory;
    }

    /**
     * @param int $id
     * @return AuthorInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): AuthorInterface
    {
        $author = $this->authorFactory->create();
        $this->authorResource->load($author,$id);

        if(!$author->getId()){
            throw new NoSuchEntityException(__("Can't find author"));
        }

        return $author;
    }

    /**
     * @param AuthorInterface $author
     * @return AuthorInterface
     * @throws CouldNotSaveException
     */
    public function save(AuthorInterface $author): AuthorInterface
    {
        try {
            $this->authorResource->save($author);
        } catch (\Exception $ex) {
            throw new CouldNotSaveException(__("Can't save Author"));
        }
        return $author;
    }

    /**
     * @param AuthorInterface $author
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(AuthorInterface $author): bool
    {
        try {
            $this->authorResource->delete($author);
        } catch (\Exception $ex) {
            throw new CouldNotDeleteException(__("Can't delete author"));
        }
        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }
}
