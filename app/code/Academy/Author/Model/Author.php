<?php

namespace Academy\Author\Model;

use Magento\Framework\Model\AbstractModel;
use Academy\Author\Api\Data\AuthorInterface;

class Author extends AbstractModel implements AuthorInterface
{
    protected $_eventPrefix = 'academy_author';
    protected $_eventObject = 'author';

    protected function _construct()
    {
        $this->_init(\Academy\Author\Model\ResourceModel\Author::class);
    }

    public function getId()
    {
        return $this->getData(AuthorInterface::AUTHOR_ID);
    }

    public function getFirstname()
    {
        return $this->getData(AuthorInterface::FIRSTNAME);
    }

    public function getLastname()
    {
        return $this->getData(AuthorInterface::LASTNAME);
    }

    public function getCreationTime()
    {
        return $this->getData(AuthorInterface::CREATION_TIME);
    }

    public function getUpdateTime()
    {
        return $this->getData(AuthorInterface::UPDATE_TIME);
    }

    public function setId($id)
    {
        return $this->setData(AuthorInterface::AUTHOR_ID, $id);
    }

    public function setFirstname($firstname)
    {
        return $this->setData(AuthorInterface::FIRSTNAME, $firstname);
    }

    public function setLastname($lastname)
    {
        return $this->setData(AuthorInterface::LASTNAME, $lastname);
    }

    public function setCreationTime($time)
    {
        return $this->setData(AuthorInterface::CREATION_TIME, $time);
    }

    public function setUpdateTime($time)
    {
        return $this->setData(AuthorInterface::UPDATE_TIME, $time);
    }
}
