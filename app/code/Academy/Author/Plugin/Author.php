<?php
namespace Academy\Author\Plugin;

use Academy\Author\Api\Data\AuthorInterface;

class Author
{
    public function beforeSetFirstname(AuthorInterface $subject, $firstname)
    {
        return ["Firstname intercepted"];
    }


    public function afterSetFirstname(AuthorInterface $subject, $result)
    {
        return $result;
//        return $result->setFirstname("Aftet Intercepted");
    }
}
