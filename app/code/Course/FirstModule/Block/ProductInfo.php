<?php

declare(strict_types=1);

namespace Course\FirstModule\Block;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

class ProductInfo extends Template
{
    const PRODUCT_SKU = "WS10";
    private ProductRepositoryInterface $productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->productRepository = $productRepository;
    }

    public function getProduct() : ?ProductInterface
    {
        try {
            return $this->productRepository->get(self::PRODUCT_SKU);
        } catch (NoSuchEntityException $exception) {
            echo __("product not found");
        }
        return null;
    }
}
