<?php

declare(strict_types=1);

namespace Academy\FirstModule\Controller\Second;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class NotIndex implements HttpGetActionInterface
{
    private ResponseInterface $response;

    /**
     * @param ResponseInterface $response
     */
    public function __construct(
        ResponseInterface $response
    )
    {
        $this->response = $response;
    }

    public function execute() : void
    {
        echo "New CONTROLLER Not index controller";
        $this->response->sendResponse();
    }
}
