<?php

namespace Course\Author\Service;

class Import
{
    private ?string $filename;

    /**
     * @param string|null $filename
     */
    public function __construct(
        string $filename = null
    )
    {
        $this->filename = $filename;
    }

    public function execute()
    {
        echo "Importing from ".$this->filename;
        echo "\n";
    }
}
