<?php

namespace Course\Author\Service;

class Importer
{
    private array $importers;

    /**
     * @param array $importers
     */
    public function __construct(
        array $importers = []
    )
    {

        $this->importers = $importers;
    }

    public function execute()
    {
        foreach ($this->importers as $import)
        {
            $import->execute();
        }
    }
}
