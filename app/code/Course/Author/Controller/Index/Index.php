<?php

declare(strict_types=1);

namespace Course\Author\Controller\Index;

use Course\Author\Api\AuthorRepositoryInterface;
use Course\Author\Api\Data\AuthorInterface;
use Course\Author\Api\Data\AuthorInterfaceFactory;
use Course\Author\Model\ResourceModel\Author;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\AlreadyExistsException;

class Index implements HttpGetActionInterface
{
    private ResponseInterface $response;
    private Author $authorResource;
    private AuthorInterfaceFactory $authorFactory;
    private AuthorRepositoryInterface $authorRepository;
    private Author\CollectionFactory $authorCollectionFactory;
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @param AuthorInterfaceFactory $authorFactory
     * @param Author $authorResource
     * @param ResponseInterface $response
     * @param AuthorRepositoryInterface $authorRepository
     * @param Author\CollectionFactory $authorCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        AuthorInterfaceFactory $authorFactory,
        Author $authorResource,
        ResponseInterface $response,
        AuthorRepositoryInterface $authorRepository,
        Author\CollectionFactory $authorCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->response = $response;
        $this->authorResource = $authorResource;
        $this->authorFactory = $authorFactory;
        $this->authorRepository = $authorRepository;
        $this->authorCollectionFactory = $authorCollectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function execute() : void
    {

        $author = $this->authorRepository->getById(2);

        $author->setName("Dan Braun");

        var_dump($author->getData());

        $this->authorRepository->save($author);

//        $collection = $this->authorCollectionFactory->create();
//
//        $collection->addFieldToFilter("name", "Franko");
//
//        $collection->getAllIds();
//
//        $collection->addFieldToFilter("author_id", [
//            "eq" => 2
//        ]);
//
//        echo $collection->getSelect()->__toString();
//
//        /** @var AuthorInterface $author */
//        foreach ($collection as $author)
//        {
//            var_dump($author->getData());
//        }

        die;
    }
}
