<?php

namespace Course\Author\Console\Command;

use Course\Author\Api\AuthorRepositoryInterface;
use Course\Author\Service\Importer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AuthorCommand extends Command
{
    private AuthorRepositoryInterface $authorRepository;
    private Importer $import;

    /**
     * @param AuthorRepositoryInterface $authorRepository
     * @param Importer $import
     * @param string|null $name
     */
    public function __construct(
        AuthorRepositoryInterface $authorRepository,
        Importer $import,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->setDescription("Author command");
        $this->authorRepository = $authorRepository;
        $this->import = $import;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->import->execute();
    }
}
