<?php

declare(strict_types=1);

namespace Course\Author\Api;

use Course\Author\Api\Data\AuthorInterface;

interface AuthorRepositoryInterface
{
    public function save(AuthorInterface $author);

    public function getById(int $id);

    public function delete(AuthorInterface $author);

    public function deleteById(int $id);

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
