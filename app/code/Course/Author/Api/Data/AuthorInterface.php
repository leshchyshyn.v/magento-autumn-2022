<?php

declare(strict_types=1);

namespace Course\Author\Api\Data;

interface AuthorInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const AUTHOR_ID     = 'author_id';
    const NAME          = 'name';
    const INFO          = 'info';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get identifier
     *
     * @return string
     */
    public function getName() : string;

    /**
     * Get title
     *
     * @return string|null
     */
    public function getInfo() : ?string;

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime() : ?string;

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime() : ?string;

    /**
     * Set ID
     *
     * @param $value
     * @return AuthorInterface
     */
    public function setId($value) : AuthorInterface;

    /**
     * Set identifier
     *
     * @param string $name
     * @return AuthorInterface
     */
    public function setName(string $name) : AuthorInterface;

    /**
     * Set title
     *
     * @param string $info
     * @return AuthorInterface
     */
    public function setInfo(string $info) : AuthorInterface;

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return AuthorInterface
     */
    public function setCreationTime(string $creationTime) : AuthorInterface;

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return AuthorInterface
     */
    public function setUpdateTime(string $updateTime) : AuthorInterface;
}
