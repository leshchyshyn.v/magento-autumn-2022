<?php

namespace Course\Author\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface AuthorSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get blocks list.
     *
     * @return AuthorInterface[]
     */
    public function getItems();

    /**
     * Set blocks list.
     *
     * @param AuthorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
