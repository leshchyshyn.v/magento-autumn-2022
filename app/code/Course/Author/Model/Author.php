<?php

declare(strict_types=1);

namespace Course\Author\Model;

use Course\Author\Api\Data\AuthorInterface;
use Magento\Framework\Model\AbstractModel;

class Author extends AbstractModel implements AuthorInterface
{
    protected $_eventPrefix = "course_author";

    protected $_eventObject = "author";

    protected function _construct()
    {
        $this->_init(\Course\Author\Model\ResourceModel\Author::class);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::AUTHOR_ID);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return (string)$this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function getInfo(): ?string
    {
        return (string)$this->getData(self::INFO);
    }

    /**
     * @inheritDoc
     */
    public function getCreationTime(): ?string
    {
        return (string)$this->getData(self::CREATION_TIME);
    }

    /**
     * @inheritDoc
     */
    public function getUpdateTime(): ?string
    {
        return (string)$this->getData(self::UPDATE_TIME);
    }

    /**
     * @inheritDoc
     */
    public function setId($value): AuthorInterface
    {
        return $this->setData(self::AUTHOR_ID, $value);
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name): AuthorInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function setInfo(string $info): AuthorInterface
    {
        return $this->setData(self::INFO, $info);
    }

    /**
     * @inheritDoc
     */
    public function setCreationTime(string $creationTime): AuthorInterface
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * @inheritDoc
     */
    public function setUpdateTime(string $updateTime): AuthorInterface
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }
}
