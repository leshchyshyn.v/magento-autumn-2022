<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Course\Author\Model;

use Course\Author\Api\Data\AuthorSearchResultsInterface;
use Magento\Cms\Api\Data\BlockSearchResultsInterface;
use Magento\Framework\Api\SearchResults;

/**
 * Service Data Object with Block search results.
 */
class AuthorSearchResults extends SearchResults implements AuthorSearchResultsInterface
{
}
