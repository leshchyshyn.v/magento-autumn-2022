<?php

declare(strict_types=1);

namespace Course\Author\Model\ResourceModel;

use Course\Author\Api\Data\AuthorInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Author extends AbstractDb
{
    protected function _construct()
    {
        $this->_init("course_author", AuthorInterface::AUTHOR_ID);
    }
}
