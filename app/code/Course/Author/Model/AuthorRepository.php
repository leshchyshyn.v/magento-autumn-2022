<?php

declare(strict_types=1);

namespace Course\Author\Model;

use Course\Author\Api\AuthorRepositoryInterface;
use Course\Author\Api\Data\AuthorInterface;
use Course\Author\Api\Data\AuthorSearchResultsInterfaceFactory;
use Course\Author\Api\Data\AuthorSearchResultsInterface;
use Course\Author\Api\Data\AuthorInterfaceFactory;
use Course\Author\Model\ResourceModel\Author as AuthorResource;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Setup\Exception;

class AuthorRepository implements AuthorRepositoryInterface
{
    private AuthorResource $authorResource;
    private AuthorInterfaceFactory $authorFactory;
    private AuthorResource\CollectionFactory $authorCollectionFactory;
    private CollectionProcessorInterface $collectionProcessor;
    private AuthorSearchResultsInterfaceFactory $searchResultsFactory;

    /**
     * @param AuthorResource $authorResource
     * @param AuthorInterfaceFactory $authorFactory
     * @param AuthorResource\CollectionFactory $authorCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param AuthorSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        AuthorResource $authorResource,
        AuthorInterfaceFactory $authorFactory,
        AuthorResource\CollectionFactory $authorCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        AuthorSearchResultsInterfaceFactory $searchResultsFactory
    )
    {
        $this->authorResource = $authorResource;
        $this->authorFactory = $authorFactory;
        $this->authorCollectionFactory = $authorCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @param AuthorInterface $author
     * @return AuthorInterface
     * @throws CouldNotSaveException
     */
    public function save(AuthorInterface $author)
    {
        try {
            $this->authorResource->save($author);
        } catch (\Exception $ex) {
            throw new CouldNotSaveException("Can't save author");
        }
        return $author;
    }

    /**
     * @param int $id
     * @return AuthorInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id)
    {
        $author = $this->authorFactory->create();
        $this->authorResource->load($author, $id);
        if(!$author->getId()) {
            throw new NoSuchEntityException("Can't load author");
        }
        return $author;
    }

    /**
     * @param AuthorInterface $author
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(AuthorInterface $author)
    {
        try {
            $this->authorResource->delete($author);
        } catch (\Exception $ex) {
            throw new CouldNotDeleteException("Could not delete author");
        }
        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Magento\Framework\DataObject[]
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        $collection = $this->authorCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
}
