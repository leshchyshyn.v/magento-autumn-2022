<?php

namespace Course\Author\Plugin;

use Course\Author\Api\Data\AuthorInterface;

class ChangeName
{
    public function beforeSetName(AuthorInterface $author, $name)
    {
        return [$name."---"];
    }
}
